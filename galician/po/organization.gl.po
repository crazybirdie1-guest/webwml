# Jorge Barreiro <yortx.barry@gmail.com>, 2012, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2014-03-16 21:28+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "correo de delegacións"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "correo de nomeamentos"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegado"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "membro"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "xestor"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Xestor da versión estable"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "mago"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "presidente"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "asistente"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "secretario"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "Dirixentes"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:87
msgid "Distribution"
msgstr "Distribución"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:229
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:232
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Publicidade"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:300
msgid "Support and Infrastructure"
msgstr "Asistencia e infraestrutura"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Debian Pure Blends"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "Comité técnico"

#: ../../english/intro/organization.data:82
msgid "Secretary"
msgstr "Secretaría"

#: ../../english/intro/organization.data:90
msgid "Development Projects"
msgstr "Proxectos de desenvolvemento"

#: ../../english/intro/organization.data:91
msgid "FTP Archives"
msgstr "Arquivos FTP"

#: ../../english/intro/organization.data:93
msgid "FTP Masters"
msgstr "Xestores do FTP"

#: ../../english/intro/organization.data:99
msgid "FTP Assistants"
msgstr "Asistentes do FTP"

#: ../../english/intro/organization.data:104
msgid "FTP Wizards"
msgstr "Magos do FTP"

#: ../../english/intro/organization.data:108
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:110
msgid "Backports Team"
msgstr "Equipo de backports"

#: ../../english/intro/organization.data:114
msgid "Individual Packages"
msgstr "Paquetes individuais"

#: ../../english/intro/organization.data:115
msgid "Release Management"
msgstr "Xestión das publicacións"

#: ../../english/intro/organization.data:117
msgid "Release Team"
msgstr "Equipo de publicación"

#: ../../english/intro/organization.data:130
msgid "Quality Assurance"
msgstr "Garantía de calidade"

#: ../../english/intro/organization.data:131
msgid "Installation System Team"
msgstr "Equipo do sistema de instalación"

#: ../../english/intro/organization.data:132
msgid "Release Notes"
msgstr "Notas da publicación"

#: ../../english/intro/organization.data:134
msgid "CD Images"
msgstr "Imaxes de CD"

#: ../../english/intro/organization.data:136
msgid "Production"
msgstr "Produción"

#: ../../english/intro/organization.data:144
msgid "Testing"
msgstr "Probas"

#: ../../english/intro/organization.data:146
msgid "Autobuilding infrastructure"
msgstr "Infraestrutura de construción automática"

#: ../../english/intro/organization.data:148
msgid "Wanna-build team"
msgstr "Equipo de peticións de construción"

#: ../../english/intro/organization.data:156
msgid "Buildd administration"
msgstr "Administración de buildd"

#: ../../english/intro/organization.data:175
msgid "Documentation"
msgstr "Documentación"

#: ../../english/intro/organization.data:180
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista de paquetes que precisan traballo e por crear"

#: ../../english/intro/organization.data:183
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:184
msgid "Ports"
msgstr "Adaptacións"

#: ../../english/intro/organization.data:219
msgid "Special Configurations"
msgstr "Configuracións especiais"

#: ../../english/intro/organization.data:222
msgid "Laptops"
msgstr "Portátiles"

#: ../../english/intro/organization.data:223
msgid "Firewalls"
msgstr "Devasas"

#: ../../english/intro/organization.data:224
msgid "Embedded systems"
msgstr "Sistemas embebidos"

#: ../../english/intro/organization.data:237
msgid "Press Contact"
msgstr "Contacto coa prensa"

#: ../../english/intro/organization.data:239
msgid "Web Pages"
msgstr "Páxinas web"

#: ../../english/intro/organization.data:249
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:254
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:258
msgid "Debian Women Project"
msgstr "Proxecto muller Debian"

#: ../../english/intro/organization.data:266
msgid "Anti-harassment"
msgstr "Anti-acoso"

#: ../../english/intro/organization.data:272
msgid "Events"
msgstr "Eventos"

#: ../../english/intro/organization.data:278
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Comité técnico"

#: ../../english/intro/organization.data:285
msgid "Partner Program"
msgstr "Programa de socios"

#: ../../english/intro/organization.data:290
msgid "Hardware Donations Coordination"
msgstr "Coordinador das doazóns de hardware"

#: ../../english/intro/organization.data:303
msgid "User support"
msgstr "Asistencia ao usuario"

#: ../../english/intro/organization.data:370
msgid "Bug Tracking System"
msgstr "Sistema de seguimento dos informes de fallos"

#: ../../english/intro/organization.data:375
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administración das listas de correo o dos seus arquivos"

#: ../../english/intro/organization.data:383
msgid "New Members Front Desk"
msgstr "Recepción de novos membros"

#: ../../english/intro/organization.data:389
msgid "Debian Account Managers"
msgstr "Xestores de contas de Debian"

#: ../../english/intro/organization.data:393
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:394
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantedores dos chaveiros (PGP e GPG)"

#: ../../english/intro/organization.data:397
msgid "Security Team"
msgstr "Equipo de seguridade"

#: ../../english/intro/organization.data:409
msgid "Consultants Page"
msgstr "Páxina de asesores técnicos"

#: ../../english/intro/organization.data:414
msgid "CD Vendors Page"
msgstr "Páxina de vendedores de CDs"

#: ../../english/intro/organization.data:417
msgid "Policy"
msgstr "Políticas"

#: ../../english/intro/organization.data:422
msgid "System Administration"
msgstr "Administración de sistemas"

#: ../../english/intro/organization.data:423
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Este é o enderezo a usar cando se atopen problemas en algunha das máquinas "
"de Debian, incluídos problemas cos contrasinais ou se precisa que se instale "
"un paquete."

#: ../../english/intro/organization.data:432
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se ten problemas co hardware de máquinas Debian, consulte a páxina <a href="
"\"https://db.debian.org/machines.cgi\">máquinas Debian</a>. Debería conter "
"información dos administradores de cada máquina."

#: ../../english/intro/organization.data:433
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador do directorio LDAP de desenvolvedores"

#: ../../english/intro/organization.data:434
msgid "Mirrors"
msgstr "Réplicas"

#: ../../english/intro/organization.data:441
msgid "DNS Maintainer"
msgstr "Mantedor do DNS"

#: ../../english/intro/organization.data:442
msgid "Package Tracking System"
msgstr "Sistema de seguimento de paquetes"

#: ../../english/intro/organization.data:444
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:450
#, fuzzy
#| msgid "<a href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "Peticións de uso da<a href=\"m4_HOME/trademark\">marca</a>"

#: ../../english/intro/organization.data:453
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Administradores de alioth"

#: ../../english/intro/organization.data:457
msgid "Alioth administrators"
msgstr "Administradores de alioth"

#: ../../english/intro/organization.data:470
msgid "Debian for children from 1 to 99"
msgstr "Debian para nenos de 1 a 99"

#: ../../english/intro/organization.data:473
msgid "Debian for medical practice and research"
msgstr "Debian para práctica e investigación médica"

#: ../../english/intro/organization.data:476
msgid "Debian for education"
msgstr "Debian para a educación"

#: ../../english/intro/organization.data:481
msgid "Debian in legal offices"
msgstr "Debian en oficinas legais"

#: ../../english/intro/organization.data:485
msgid "Debian for people with disabilities"
msgstr "Debian para persoas con discapacidades"

#: ../../english/intro/organization.data:489
msgid "Debian for science and related research"
msgstr "Debian para investigación científica e relacionada"

#: ../../english/intro/organization.data:492
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian para a educación"

#~ msgid "Testing Security Team"
#~ msgstr "Equipo de seguridade en testing"

#~ msgid "Security Audit Project"
#~ msgstr "Proxecto de auditoría de seguridade"

#~ msgid "current Debian Project Leader"
#~ msgstr "Actual líder do proxecto Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Presidencias DebConf"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Mantedores do chaveiro de mantedores de Debian (DM)"

#~ msgid "Bits from Debian"
#~ msgstr "Novas breves de Debian"

#~ msgid "Publicity"
#~ msgstr "Publicidade"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Live System Team"
#~ msgstr "Equipo do sistema Live"
